import { Body, Controller, Post } from '@nestjs/common';
import { CredencialesDTO } from 'src/usuario/dto/credenciales.dto';
import { UsuarioService } from 'src/usuario/usuario.service';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private usuarioService:UsuarioService,
        private authService: AuthService,
        ){}
    //creamos la ruta
    @Post('login')
   async login(@Body() credencialesDTO:CredencialesDTO){
    console.log('credenciales del cuerpo', credencialesDTO);
        const usuario=await this.usuarioService.validarUsuario(
            credencialesDTO.nombreUsuario,
            credencialesDTO.password,
        );
    return this.authService.login(usuario);
    }
}
