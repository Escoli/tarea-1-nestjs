import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { Usuario } from 'src/usuario/entities/usuario.entity';
import { UsuarioService } from 'src/usuario/usuario.service';
import { UsuarioRepository } from 'src/usuario/usuario.repository';
import { jwtConstants } from 'src/constantes/constant';
import { JwtStrategy } from './jwt.strategy';

@Module({
    imports:[
        TypeOrmModule.forFeature([Usuario]), 
        UsuarioModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions:{expiresIn:'60s'},
        }),
    ],
    providers: [AuthService, UsuarioService, UsuarioRepository, JwtStrategy],
    controllers: [AuthController],
    })
//crear una autenticacion que funcione dentro de  nuestra aplicacion
export class AuthModule {}
