import { IsEmail, IsNotEmpty, IsString, MinLength, IsAlphanumeric, minLength, isNumber, IsNumber } from "class-validator";

export class CreateUsuarioDto {
    @IsNumber()
    @IsNotEmpty()
    id: number;

    @IsString()
    @IsNotEmpty()
    @MinLength(5,
        {message: 'El nombre de usuario debería tener al menos 5 caracteres',
    })
    nombre: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(3,
        {message: 'El nickname de usuario debería tener al menos 3 caracteres',
    })
    @IsAlphanumeric(null, {message: 'Solo se permiten numeros y  letras'})
    nombreUsuario: string;

    @IsString()
    @IsNotEmpty()
    @IsEmail(null, {message: 'Ingrese un emaiñ válido'})    
    email: string;

    @IsString()
    @IsNotEmpty()
    password: string;

}
