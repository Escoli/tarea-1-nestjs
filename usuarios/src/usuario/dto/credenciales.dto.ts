import{ IsNotEmpty, IsString, MinLength } from "class-validator";

export class CredencialesDTO{
    @IsString()
    @IsNotEmpty()
   // @IsEmail()
    @MinLength(3,{
    message: 'El nombre del usuario deberia tener al menos 3 carateres'
    })
    nombreUsuario: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(6,{
        message: 'La contraseña deberia tener al menos 5 carateres'
    })
    password: string;
}