import { 
  Controller, 
  Get, 
  Post, 
  Body, 
  Patch, 
  Param, 
  Delete, 
  UseGuards} from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';


@UseGuards(JwtAuthGuard) //para  proteger a todas las funciones
@Controller('usuarios')
export class UsuarioController {
  constructor(
  private readonly usuarioService: UsuarioService,
  ){}

  @Post()
  create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return this.usuarioService.create(createUsuarioDto);
  }

  @Get('/:id')
  findOne(@Param('id') id: number) {
    return this.usuarioService.findOne(id);
  }

  @UseGuards(JwtAuthGuard) //solo esta funcion sera la que proteja
  @Get()
  findAll() {
    return this.usuarioService.findAll();
  }

  @Patch('/:id')
  update(@Param('id') id: number, @Body() updateUsuarioDto: UpdateUsuarioDto) {
    return this.usuarioService.actualizar(id, updateUsuarioDto);
  } 

  @Delete('/:id')
  remove(@Param('id') id: number) {
    return this.usuarioService.eliminar(id);
  }

/*   @Post('ingresar')
  ingresar(
    @Body() credenciales: CredencialesDTO){
    const resultado=this.usuarioService.acceder(credenciales)
    return resultado
  }  */
}
