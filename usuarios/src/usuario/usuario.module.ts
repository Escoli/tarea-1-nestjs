import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
import { TypeOrmModule} from '@nestjs/typeorm';
import { Usuario } from './entities/usuario.entity';
import { UsuarioRepository } from './usuario.repository';


@Module({
  imports:[TypeOrmModule.forFeature([Usuario])], 
  controllers: [UsuarioController],
  providers: [UsuarioService, UsuarioRepository],
}) 
export class UsuarioModule {}
/* implements NestModule {
configure(consumer: MiddlewareConsumer) {
    //consumer.apply(LoggerMiddleware).forRoutes('usuarios');     //1 forma
    consumer.apply(LoggerMiddleware).forRoutes({path: 'usuarios/listar', method: RequestMethod.GET});  //Segunda forma
 }
}
*/
