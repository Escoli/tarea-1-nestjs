import { Inject, Injectable } from "@nestjs/common";
import { Usuario } from "./entities/usuario.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateUsuarioDto } from "./dto/create-usuario.dto";
import { UpdateUsuarioDto } from "./dto/update-usuario.dto";


@Injectable()
export class UsuarioRepository{
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>
    ){}
    crear(createUsuarioDto: CreateUsuarioDto){
       // const usuario=new Usuario({...createUsuarioDto});
       //return this.usuarioRepository 
        return this.usuarioRepository.save(createUsuarioDto);
    }
    buscarPorId(id:number){
        //const usuario=this.dataSource.getRepository;
       return this.usuarioRepository.findOneBy({id});
    }
    async buscarPorNombre(nombreUsuario: string): Promise<Usuario>{
        return await this.usuarioRepository.findOne({where:{nombreUsuario}})
    }
    listar(){
        //const usuario=this.dataSource.getRepository;
       return this.usuarioRepository.find();
    }

    actualizar(id:number, updateUsuarioDto: UpdateUsuarioDto){
        //const usuario=this.dataSource.getRepository;
       return this.usuarioRepository.update(id, updateUsuarioDto)
    } 
    eliminar(id: number){
        return this.usuarioRepository.delete(id);
    }
}