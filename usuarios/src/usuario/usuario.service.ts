import { ConflictException, Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UsuarioRepository } from './usuario.repository';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';
import { CredencialesDTO } from './dto/credenciales.dto';

@Injectable()
export class UsuarioService {
  acceder(credenciales: CredencialesDTO) {
    throw new Error('Method not implemented.');
  }
  constructor(
        private readonly usuarioRepository: UsuarioRepository){}
  /*create(createUsuarioDto: CreateUsuarioDto) {
    const usuario: Usuario={
      id:createUsuarioDto.id,
      nombre:createUsuarioDto.nombre,
      nombreUsuario:createUsuarioDto.nombreUsuario,
      email:createUsuarioDto.email,s
      password:createUsuarioDto.password,
    };*/
  async create(createUsuarioDto:CreateUsuarioDto): Promise<Usuario>{
    //crear una consulta por id o por nombre vayan a ser creados o no
    const usuarioExistente=this.usuarioRepository.buscarPorNombre(
      createUsuarioDto.nombreUsuario,
      );
      if(usuarioExistente){
        throw new ConflictException('Usuario Existente');
      }
    return this.usuarioRepository.crear(createUsuarioDto);    
  }

  async validarUsuario(
    nombreUsuario: string, 
    password:string,
    ): Promise<Usuario>{
    const usuarioExistente=
      await this.usuarioRepository.buscarPorNombre(nombreUsuario);
       console.log('usuario Existente',usuarioExistente);
    if(!usuarioExistente || usuarioExistente.password!==password ||
       usuarioExistente.nombreUsuario!==nombreUsuario) {
      throw new UnauthorizedException(
        'Nombre de Usuario o Contraeña incorrectos',
      );
    }
    return usuarioExistente;
  }
  
  findOne(id: number){
      return this.usuarioRepository.buscarPorId(id); 
  }
  
  findAll() {
    
    return this.usuarioRepository.listar();
  }


  actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const usuario=this.usuarioRepository.buscarPorId(id);
    if(!usuario){
      throw new Error('Usuario con id ${id} no se ha encontrado');
    }
      return this.usuarioRepository.actualizar(id,updateUsuarioDto);   
} 

  eliminar(id: number) {
    return this.usuarioRepository.eliminar(id);
  }
}

